function mapObject(obj, cb){
    const modifiedObj={};
    for (let key in obj){
        const value=obj[key];
        let modifiedValue=cb(value);
        modifiedObj[key]=modifiedValue;
    }
    return modifiedObj;
}


function cb(value){
    let newValue=value+10;
    return newValue;
}

module.exports={mapObject,cb};