function values(obj){
    const allValuesArray=[];
    for (let key in obj){
        let value=obj[key];
        allValuesArray.push(value);
    } 
    return allValuesArray;
}

module.exports=values;