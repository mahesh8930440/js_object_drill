function pairs(obj){
    const allKeysAndValues=[]
    for (let key in obj){
        let value=obj[key];
        const keyAndValueArray=[key,value]
        allKeysAndValues.push(keyAndValueArray);
    }
    return allKeysAndValues;
}

module.exports=pairs;